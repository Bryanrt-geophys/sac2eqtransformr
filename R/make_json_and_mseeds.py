def make_json_and_mseeds(s_date, s_time, e_date, e_time):
  start_time = "%s %s" % (s_date, s_time)
  end_time = "%s %s" % (e_date, e_time)
  json_basepath = os.path.join(os.getcwd(),"json/station_list.json")
  makeStationList(json_path=json_basepath,
                  client_list=["IRIS"], 
                  min_lat=31.00, 
                  max_lat=33.50, 
                  min_lon=-105.00, 
                  max_lon=-102.00, 
                  start_time= start_time, 
                  end_time=end_time, 
                  channel_list=["HHZ", "HHE", "HHN", "EHZ", "HH1", "HH2", "EHE", "EHN", "EH1", "EH2"], 
                  filter_network=["SY", "PN"], 
                  filter_station=["DG01", "DG02", "DG03", "DG04", "DG05", "DG06", "DG07", "DG08", "DG09", 
                                  "MB01", "MB02", "MB04", "MB07",
                                   "PB03", "PB04", "PB06", "PB02", "PB09", "PB10", "PB12", "PB13", "PB14", "PB15",
                                    "PB16", "PB18", "PB19", "PB21", "PB28", "PB29", "PB30", "PB31", "PB32", "PB33"])
  downloadMseeds(client_list=["IRIS"], 
                 stations_json=json_basepath, 
                 output_dir="downloads_mseeds", 
                 min_lat=31.00, 
                 max_lat=33.50, 
                 min_lon=-105.00, 
                 max_lon=-102.00, 
                 start_time=start_time, 
                 end_time=end_time, 
                 chunk_size=1, 
                 channel_list=["HHZ", "HHE", "HHN", "EHZ", "HH1", "HH2", "EHE", "EHN", "EH1", "EH2"], 
                 n_processor=4)
  return "signal collected"
