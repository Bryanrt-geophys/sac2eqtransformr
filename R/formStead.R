#' form_STEAD
#'
#' Reformats attributes for HDF5 and csv format
#'
#' @param STEAD_data output from joining_sac2STEAD
#' @param meta_data output from form_META
#'
#' @importFrom dplyr %>%
#'
#' @export

form_STEAD <- function(STEAD_data, meta_data){

  progressr::with_progress({
    p <- progressr::progressor(steps = 7)

    for (i in 1:length(STEAD_data$trace_name)) {
      meta_data$coda_end_sample[i] = STEAD_data$coda_end_sample[i]
    }

    p()
    Sys.sleep(.2)

    STEAD_csv <- meta_data
    STEAD_csv$snr_db <- STEAD_csv$snr_db %>% as.character()

    STEAD_csv <- STEAD_csv %>%
      dplyr::mutate(
        network_code = dplyr::if_else(network_code != "NULL", as.character(network_code), ""),
        receiver_code = dplyr::if_else(receiver_code != "NULL", as.character(receiver_code), ""),
        receiver_type = dplyr::if_else(receiver_type != "NULL",as.character(receiver_type), ""),      # manually defined, not sure if BH is correct though.
        receiver_latitude = dplyr::if_else(receiver_latitude != "NULL",as.character(receiver_latitude), ""),
        receiver_longitude = dplyr::if_else(receiver_longitude != "NULL",as.character(receiver_longitude), ""),
        receiver_elevation_m = dplyr::if_else(receiver_elevation_m != "NULL",as.character(receiver_elevation_m), ""),
        p_arrival_sample = dplyr::if_else(p_arrival_sample != "NULL",as.character(p_arrival_sample), ""),
        p_status = dplyr::if_else(p_status != "NULL",as.character(p_status), ""),
        p_weight = dplyr::if_else(p_weight != "NULL",as.character(p_weight), ""),
        p_travel_sec = dplyr::if_else(p_travel_sec != "NULL",as.character(p_travel_sec), ""),
        s_arrival_sample = dplyr::if_else(s_arrival_sample != "NULL",as.character(s_arrival_sample), ""),
        s_status = dplyr::if_else(s_status != "NULL",as.character(s_status), ""),
        s_weight = dplyr::if_else(s_weight != "NULL",as.character(s_weight), ""),
        source_id = dplyr::if_else(source_id != "NULL",as.character(source_id), ""),     # is this a user defined index, or does this come from IRIS?
        source_origin_time = dplyr::if_else(source_origin_time != "NULL",as.character(source_origin_time), ""),
        source_origin_uncertainty_sec = dplyr::if_else(source_origin_uncertainty_sec != "NULL",as.character(source_origin_uncertainty_sec), ""),
        source_latitude = dplyr::if_else(source_latitude != "NULL",as.character(source_latitude), ""),
        source_longitude = dplyr::if_else(source_longitude != "NULL",as.character(source_longitude), ""),
        source_error_sec = dplyr::if_else(source_error_sec != "NULL",as.character(source_error_sec), ""),
        source_gap_deg = dplyr::if_else(source_gap_deg != "NULL", as.character(source_gap_deg), ""),          # check this variables source to see the unit. deg or m?
        source_horizontal_uncertainty_km = dplyr::if_else(source_horizontal_uncertainty_km != "NULL",as.character(source_horizontal_uncertainty_km), ""),   # is this an std_1 value from statistical eval? May be in the dat files.
        source_depth_km = dplyr::if_else(source_depth_km != "NULL",as.character(source_depth_km), ""),
        source_depth_uncertainty_km = dplyr::if_else(source_depth_uncertainty_km != "NULL",as.character(source_depth_uncertainty_km), ""),     # is this an std_1 value from statistical eval? May be in the dat files.
        source_magnitude = dplyr::if_else(source_magnitude != "NULL",as.character(source_magnitude), ""),
        source_magnitude_type = dplyr::if_else(source_magnitude_type != "NULL",as.character(source_magnitude_type), ""),
        source_magnitude_author = dplyr::if_else(source_magnitude_author != "NULL",as.character(source_magnitude_author), ""),
        source_mechanism_strike_dip_rake = dplyr::if_else(source_mechanism_strike_dip_rake != "NULL",as.character(source_mechanism_strike_dip_rake), ""),
        source_distance_deg = dplyr::if_else(source_distance_deg != "NULL",as.character(source_distance_deg), ""),
        source_distance_km = dplyr::if_else(source_distance_km != "NULL",as.character(source_distance_km), ""),
        back_azimuth_deg = dplyr::if_else(back_azimuth_deg != "NULL",as.character(back_azimuth_deg), ""),
        snr_db = dplyr::if_else(snr_db != "NULL",as.character(snr_db) %>% stringr::str_remove_all(., c(",")) %>% stringr::str_remove_all(c("c")) %>% gsub("[(]", "[", .) %>% gsub("[)]", "]", .), ""),
        coda_end_sample = dplyr::if_else(is.na(coda_end_sample) == FALSE, paste0("[[",ceiling(coda_end_sample), ".]]"), ""),
        trace_start_time = dplyr::if_else(trace_start_time != "NULL",as.character(trace_start_time), ""),
        trace_category = dplyr::if_else(trace_category != "NULL",as.character(trace_category), ""),
        trace_name = dplyr::if_else(trace_name != "NULL",as.character(trace_name), "")
      )
    # %>% dplyr::arrange(trace_name)

    if (file.exists(here::here("NMTSO_meta.csv")) == T) {
      STEAD_predicessor <- readr::read_csv(file = here::here("NMTSO_meta.csv"), col_names = T)
      STEAD_predicessor <- STEAD_predicessor %>%
        dplyr::mutate(
          network_code = as.character(network_code),
          receiver_code =  as.character(receiver_code),
          receiver_type = as.character(receiver_type),      # manually defined, not sure if BH is correct though.
          receiver_latitude = as.character(receiver_latitude),
          receiver_longitude = as.character(receiver_longitude),
          receiver_elevation_m = as.character(receiver_elevation_m),
          p_arrival_sample = as.character(p_arrival_sample),
          p_status = as.character(p_status),
          p_weight = as.character(p_weight),
          p_travel_sec = as.character(p_travel_sec),
          s_arrival_sample = as.character(s_arrival_sample),
          s_status = as.character(s_status),
          s_weight = as.character(s_weight),
          source_id = as.character(source_id),     # is this a user defined index, or does this come from IRIS?
          source_origin_time = as.character(source_origin_time),
          source_origin_uncertainty_sec = as.character(source_origin_uncertainty_sec),
          source_latitude = as.character(source_latitude),
          source_longitude = as.character(source_longitude),
          source_error_sec = as.character(source_error_sec),
          source_gap_deg = as.character(source_gap_deg),          # check this variables source to see the unit. deg or m?
          source_horizontal_uncertainty_km = as.character(source_horizontal_uncertainty_km),   # is this an std_1 value from statistical eval? May be in the dat files.
          source_depth_km = as.character(source_depth_km),
          source_depth_uncertainty_km = as.character(source_depth_uncertainty_km),     # is this an std_1 value from statistical eval? May be in the dat files.
          source_magnitude = as.character(source_magnitude),
          source_magnitude_type = as.character(source_magnitude_type),
          source_magnitude_author = as.character(source_magnitude_author),
          source_mechanism_strike_dip_rake = as.character(source_mechanism_strike_dip_rake),
          source_distance_deg = as.character(source_distance_deg),
          source_distance_km = as.character(source_distance_km),
          back_azimuth_deg = as.character(back_azimuth_deg),
          snr_db = as.character(snr_db),
          coda_end_sample = as.character(coda_end_sample),
          trace_start_time = as.character(trace_start_time),
          trace_category = as.character(trace_category),
          trace_name = as.character(trace_name)
        )

      STEAD_csv <- STEAD_csv %>% bind_rows(STEAD_predicessor)
      rm(STEAD_predicessor)
    }


    STEAD_csv$snr_db[which(is.na(meta_data$snr_db %>% lapply(`[[`, 1)) == T)] = "[True True True]"

    readr::write_csv(x = STEAD_csv, file = here::here("NMTSO_meta.csv"), append = FALSE, col_names = TRUE)

    meta_data
  })
}
