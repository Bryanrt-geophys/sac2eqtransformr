def make_eqt_hdf5(h5_file_path, STEAD_meta, x, info):
  e = h5py.File(''+r.h5_file_path, 'a')
  for i in np.r_[0:int(len(r.STEAD_meta.trace_name))]:
    e.create_dataset(name="data/"+r.STEAD_meta.trace_name[i],
    shape=r.x[i].T.shape,
    data=r.x[i].T,
    dtype=np.float32)
  for i in np.r_[0:len(r.STEAD_meta.trace_name)]:
    for j in np.r_[(35*i):(35*i+35)]:
      if r.info.col_names[j] == 'snr_db':
        e['data/'+r.STEAD_meta.trace_name[i]].attrs[''+r.info.col_names[j]] = np.array(r.info.value[j]).reshape(3,)
      elif r.info.col_names[j] == 'coda_end_sample':
        e['data/'+r.STEAD_meta.trace_name[i]].attrs[''+r.info.col_names[j]] = np.array(r.info.value[j]).reshape(1,1)
      else :
        e['data/'+r.STEAD_meta.trace_name[i]].attrs[''+r.info.col_names[j]] = r.info.value[j]
  e.close()
  return "hdf5 file written"
